package tema1;

public class Monom {
	private float coefficient;
	private int degree;
	
	Monom(float coeff, int degr){
		
		this.coefficient = coeff;
		this.degree = degr;
		
	}
	
	public float getCoefficient(){
		
		return coefficient;
	}
	
	public int getDegree(){
		
		return degree;
	}
	
	public void setCoefficient(float x){
		
		this.coefficient = x;
	}
	
	public void setDegree(int y){
		
		this.degree = y;
	}
}
