package tema1;

import java.util.ArrayList;

public class Polinom {
	
	 ArrayList <Monom> pol = new ArrayList <Monom>();
	
	
	static Polinom res = new Polinom();
	static Polinom res1 = new Polinom();
	Monom m1;
	Monom m2;
	Monom m3;
	
	
	public void addMonom(float coefficient, int degree){
		
		Monom m = new Monom(coefficient, degree);
		pol.add(m);
		
	}
	
	
	 public Polinom addPolinom(Polinom pol2){
		 
		 for (Monom i : this.pol){
				for (Monom j : pol2.pol){
					if (i.getDegree() == j.getDegree()){
						i.setCoefficient(i.getCoefficient() + j.getCoefficient());
						j.setDegree(-1);
					}
				}
			}
			
			for (Monom j : pol2.pol){
				if (j.getDegree() != -1){
					this.addMonom(j.getCoefficient(), j.getDegree());
				}
			}
			
			for (Monom i : this.pol){
				if (i.getCoefficient() != 0){
					res.addMonom(i.getCoefficient(), i.getDegree());
				}
			}
		
		return res;
	}
	
	 public Polinom subPolinom(Polinom pol2){
		 
		 for (Monom i : this.pol){
				for (Monom j : pol2.pol){
					if (i.getDegree() == j.getDegree()){
						i.setCoefficient(i.getCoefficient() - j.getCoefficient());
						j.setDegree(-1);
					}
				}
			}
			
			for (Monom j : pol2.pol){
				if (j.getDegree() != -1){
					this.addMonom(-j.getCoefficient(), j.getDegree());
				}
			}
			
			for (Monom i : this.pol){
				if (i.getCoefficient() != 0){
					res.addMonom(i.getCoefficient(), i.getDegree());
				}
			}
	
		return res;
	 }
	 
	 public Polinom mulPolinom(Polinom pol2){
		 
		 for (Monom i : this.pol){
				for (Monom j : pol2.pol){
						res.addMonom(i.getCoefficient() * j.getCoefficient(), i.getDegree() + j.getDegree() );
				}
			}
		 
		return res;
	 }
	 
	 
	 public Polinom derivPolinom(){
		 
		 for (Monom i : this.pol){
			 if (i.getDegree() != 0){
				 res.addMonom(i.getCoefficient() * i.getDegree(), i.getDegree() - 1);
			 }
			 else{
				 res.addMonom(0, 0);
			 }
		 }
		 
		 return res;
		 
	 }
	 
	 public Polinom integrPolinom(){
		 
		 for (Monom i : this.pol){
			 res.addMonom(i.getCoefficient() / (i.getDegree() + 1), i.getDegree() + 1);
			 
		 }

		 return res;
		 
	 }
	 
	public String toString(){
		String s = new String("");
		
		for(Monom i : this.pol){
			
			String x = s;
			s = x + "+" + String.valueOf(i.getCoefficient()) + "X^" + String.valueOf(i.getDegree());
	
		}
		
		return s;
	}
	
}
