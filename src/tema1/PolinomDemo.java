package tema1;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class PolinomDemo extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private JButton button1 = new JButton("+");
	private JButton button2 = new JButton("-");
	private JButton button3 = new JButton("*");
	private JButton button4 = new JButton("deriv");
	private JButton button5 = new JButton("integr");
	private JTextField text1 = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	private JLabel label1 = new JLabel("Primul polinom:");
	private JLabel label2 = new JLabel("Al doilea polinom:");
	private JLabel label3 = new JLabel("Rezultat:");

	
	public PolinomDemo(String name) {
		super(name);
		c.gridx = 1;
		c.gridy = 3;
		pane.add(button1, c);
		button1.addActionListener(this);
		
		c.gridx = 2;
		c.gridy = 3;
		pane.add(button2, c);
		button2.addActionListener(this);
		
		c.gridx = 3;
		c.gridy = 3;
		pane.add(button3, c);
		button3.addActionListener(this);
		
		c.gridx = 4;
		c.gridy = 3;
		pane.add(button4, c);
		button4.addActionListener(this);
		
		c.gridx = 5;
		c.gridy = 3;
		pane.add(button5, c);
		button5.addActionListener(this);

		c.gridx = 0;
		c.gridy = 0;
		pane.add(label1, c);

		c.gridx = 0;
		c.gridy = 1;
		pane.add(text1, c);
		
		c.gridx = 0;
		c.gridy = 2;
		pane.add(label2, c);
		
		c.gridx = 0;
		c.gridy = 3;
		pane.add(text2, c);
		
		c.gridx = 0;
		c.gridy = 5;
		pane.add(label3, c);
		
		this.add(pane);
		
		

	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		Object source = arg0.getSource();
		
		
		String p1 = text1.getText().toString();
		Polinom pol1 = new Polinom();
	
		String p2 = text2.getText().toString();
		Polinom pol2 = new Polinom();
		
			
		Pattern pattern = Pattern.compile("(-?\\d+)[X]\\^(-?\\d+)");
		Matcher matcher1 = pattern.matcher(p1);
		while (matcher1.find()) {
			pol1.addMonom( Integer.parseInt(matcher1.group(1)), Integer.parseInt(matcher1.group(2)));
			
		}
		
		Pattern pattern1 = Pattern.compile("(-?\\d+)[X]\\^(-?\\d+)");
		Matcher matcher2 = pattern1.matcher(p2);
		while (matcher2.find()) {
			pol2.addMonom( Integer.parseInt(matcher2.group(1)), Integer.parseInt(matcher2.group(2)));
		}
		
		if(source == button1){
	        
			String str =  pol1.addPolinom(pol2).toString();	
			label3.setText(str);
			
		}
		
		if(source == button2){
			
			String str =  pol1.subPolinom(pol2).toString();	
			label3.setText(str);
		}
		
		if(source == button3){
			label3.setText("");
			String str =  pol1.mulPolinom(pol2).toString();	
			label3.setText(str);
		}
		
		if(source == button4){
			
			String str = pol1.derivPolinom().toString();	
			label3.setText(str);
		}
		if(source == button5){
			
			String str =  pol1.integrPolinom().toString();	
			label3.setText(str);
		}
		
	}

	public static void main(String args[]) {
		
		JFrame frame = new PolinomDemo("Tema 1 - Polinoame");
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}


	
	

	
	

}
