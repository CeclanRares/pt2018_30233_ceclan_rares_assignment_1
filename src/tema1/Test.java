package tema1;

import static org.junit.Assert.*;

public class Test {

	@org.junit.Test
	public void testAdd() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		p1.addMonom(1, 1);
		p1.addMonom(1, 0);
		p2.addMonom(1, 0);
		Polinom res = new Polinom();
		res.addMonom(1,1);
		res.addMonom(2, 0);
		Polinom res2 = new Polinom();
		res2 = p1.addPolinom(p2);
		if (res.pol.size() != res2.pol.size()) {
			assertEquals(1,0);
		}
		else {
			for (int i=0; i<res.pol.size(); i++)
			{
				assertEquals((int)res.pol.get(i).getCoefficient(), (int)res2.pol.get(i).getCoefficient());
				assertEquals(res.pol.get(i).getDegree(),res2.pol.get(i).getDegree());
			}
		}
	}
	
	@org.junit.Test
	public void testMul() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		p1.addMonom(1, 1);
		p1.addMonom(1, 0);
		p2.addMonom(1, 0);
		Polinom res = new Polinom();
		res.addMonom(1,1);
		res.addMonom(2, 0);
		res.addMonom(1,1);
		res.addMonom(1, 0);
		Polinom res2 = new Polinom();
		res2 = p1.mulPolinom(p2);
		
		if (res.pol.size() != res2.pol.size()) {
			assertEquals(1,0);
		}
		else {
			for (int i=0; i<res.pol.size(); i++)
			{
				assertEquals((int)res.pol.get(i).getCoefficient(), (int)res2.pol.get(i).getCoefficient());
				assertEquals(res.pol.get(i).getDegree(),res2.pol.get(i).getDegree());
			}
		}
	}
	
	@org.junit.Test
	public void testSub() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		p1.addMonom(1, 1);
		p1.addMonom(1, 0);
		p2.addMonom(1, 0);
		Polinom res = new Polinom();
		res.addMonom(1,1);
		res.addMonom(2, 0);
		res.addMonom(1,1);
		res.addMonom(1, 0);
		res.addMonom(1,1);
		
		Polinom res2 = new Polinom();
		res2 = p1.subPolinom(p2);
		
		if (res.pol.size() != res2.pol.size()) {
			assertEquals(1,0);
		}
		else {
			for (int i=0; i<res.pol.size(); i++)
			{
				assertEquals((int)res.pol.get(i).getCoefficient(), (int)res2.pol.get(i).getCoefficient());
				assertEquals(res.pol.get(i).getDegree(),res2.pol.get(i).getDegree());
			}
		}
	}
	
	
	
	@org.junit.Test
	public void testDeriv() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		p1.addMonom(1, 1);
		p1.addMonom(1, 0);
		p2.addMonom(1, 0);
		Polinom res = new Polinom();
		res.addMonom(1,1);
		res.addMonom(2, 0);
		res.addMonom(1,1);
		res.addMonom(1, 0);
		res.addMonom(1,1);
		res.addMonom(1, 0);
		res.addMonom(0, 0);
		
		Polinom res2 = new Polinom();
		res2 = p1.derivPolinom();
		
		if (res.pol.size() != res2.pol.size()) {
			assertEquals(1,0);
		}
		else {
			for (int i=0; i<res.pol.size(); i++)
			{
				assertEquals((int)res.pol.get(i).getCoefficient(), (int)res2.pol.get(i).getCoefficient());
				assertEquals(res.pol.get(i).getDegree(),res2.pol.get(i).getDegree());
			}
		}
	}
	
	@org.junit.Test
	public void testIntegr() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		p1.addMonom(1, 1);
		p1.addMonom(1, 0);
		p2.addMonom(1, 0);
		Polinom res = new Polinom();
		res.addMonom(1,1);
		res.addMonom(2, 0);
		res.addMonom(1,1);
		res.addMonom(1, 0);
		res.addMonom(1,1);
		res.addMonom(1, 0);
		res.addMonom(0, 0);
		res.addMonom(0.5f, 2);
		res.addMonom(1, 1);
		
		Polinom res2 = new Polinom();
		res2 = p1.integrPolinom();
		
		if (res.pol.size() != res2.pol.size()) {
			assertEquals(1,0);
		}
		else {
			for (int i=0; i<res.pol.size(); i++)
			{
				assertEquals((int)res.pol.get(i).getCoefficient(), (int)res2.pol.get(i).getCoefficient());
				assertEquals(res.pol.get(i).getDegree(),res2.pol.get(i).getDegree());
			}
		}
	}
}
